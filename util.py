import configparser

file = 'config.properties'


def get_conf_property(sec, prop):
    try:
        data = None
        config = configparser.ConfigParser()
        config.read(file)
        if sec in config:
            data = config.get(sec, prop)
        return data
    except Exception as e:
        return e