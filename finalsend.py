from flask import Flask
from flask import request
from gensim.summarization import summarize
import demofile

app = Flask(__name__)


#@app.route("/")
#def hello():
 #   return "Home page"


@app.route('/summary', methods=['POST'])
def summary():
    req = request.get_json()
    print(req)
    if 'email_id' in req:
        email_id = req.get("email_id")
    if 'document' in req:
        document = req.get("document")
    # document = "Max- Hi everyone. The gd topic for discussion is Artificial Intelligence - Pros and Cons. Artificial Intelligence is a concept which refers to the programming which can make machines intelligent. In simpler terms, the program or machine is made in such a way that it keeps on learning with whatever output the machine creates. In my opinion, artificial intelligence is a boon to humanity as it can vastly create more opportunities for people.Emma- Hi everyone. As far as my opinion is concerned, the use of machine learning or artificial intelligence should be restricted and should not be made a way of life. It can have serious repercussions if machines become more intelligent than humans. In a limited environment, machines can prove beneficial but human intelligence should also remain in control on machines.Peter- Well, that holds true for everything that there should be a balance. But improvements in artificial intelligence can create a world of opportunities. Consider the example of driverless cars which are being created by the leading tech companies in the world. With the sheer use of machines, programmed software, AI etc driving cars would become automated. If all cars are running on AI it could cause reduction in traffic accidents, speed control and there would be no human errors. People often get tired or exhausted or lose concentration while driving. But with driverless cars, there would be no such situation. Hence there is definitely a strong merit in artificial intelligence.Stacy- Hi. In a few scenarios, it may seem a good option. But what about unemployment? Increasing machines and technological advancements have always created unemployment. The work which was earlier done by 50 people if it is done by a single AI based machine, it would create lack of jobs. And if this is expanded across business sectors and is increased over years, it could create a massive problem for the human race as there would unemployment and hence more poverty, hunger and related issues.Emma- Exactly. All the jobs which humans do if done by machines would lead to mass unemployment and eventually destabilise economic growth of countries.Max- I beg to differ. With the technological advancements which has been created because of artificial intelligence. For eg websites which help users search online or relevant ads on websites etc have helped boost business and sales. In fact, a lot of new opportunities have also been created because of AI in businesses. Efficiencies have gone up, errors have reduced, timelines of deliveries have shortened, and all this was possible because of machine learning.Adrian- There has to be a good balance between how and where to use artificial intelligence. Eventually I feel that humans should have the final control over whatever machine learning or artificial intelligence program is being created. Because no machine can ever have feelings or passion as a human possesses.Peter- Healthcare has vastly been benefited by AI. There has been better forecasting, trend analysis, symptom reading etc which has enabled the doctors to take much by critical decisions within a curable span of time. Also, use of machine learning in banking has enabled to reduce frauds as irregularities can be caught urgently and bank staff can intervene on priority.Emma- These interventions are fine. But can a machine replace the human touch and be compassionate? A machine can solve technical queries but can it resolve issues dealing with human stress?Adrian- Currently the answer is no but it can be developed in the near future. As we are aware the robot named Sophia was given the citizenship of Saudi Arabia. The robot was able to interact with humans based on basic conversations. If moulded properly, it can create a great opportunity.Max- As much as we deny, even today we are surrounded by Artificial Intelligence. Websites suggesting best products or placing orders for pizza over call with IVR or asking a program about weather is all a sign of machine learning. If we look at it from a development perspective, humans and machines are together solving problems and that too at a faster rate.Peter- Thats true. Companies and governments are investing billions of dollars in ensuring more machine learning based tools which would calculate, evaluate and give the best solutions to human problems, which can help streamline things like water, electricity etc distribution.Adrian- The drawback however of more artificial intelligence would be devaluing the human brain. Because a human would always make mistakes, get exhausted and would forget things. Whereas a machine would never face these issues and would eventually replace people.Artificial intelligence is a piece of science which focuses on making a machine more intelligent by programming it for constant learning. There have been several innovations in this field which have reduced human effort and help give accurate & quick solutions to human problems. However, the drawback is that too much dependence on machines would eliminate the need of humans in business, and could lead to serious issues like unemployment, poverty, economic instability etc."
   # print(document)
    summarize(document, ratio=0.4)
    fh = open("summary_file.txt", "w+")
    fh.write(summarize(document,ratio=0.4))
    fh.close()

    demofile.send_summary_email(email_id , "summary_file.txt")
    return {"status_code": 200, "result": "the summary is generated and the email has been sent."}

if __name__ == "__main__":
    app.run()