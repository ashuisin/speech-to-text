package com.injava.config;

public abstract class AbstractAnnotationConfigDispatcherServletInitializer {
    protected abstract Class[] getServletConfigClasses();

    protected abstract String[] getServletMappings();
}
