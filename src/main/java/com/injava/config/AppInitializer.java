package com.injava.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

@Configuration
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    public java.lang.Class<?>[] getRootConfigClasses() {
        java.lang.Class<AppConfig>[] classes = new java.lang.Class[]{AppConfig.class};
        return classes;
    }

    @Override
    protected java.lang.Class<?>[] getServletConfigClasses() {
        return new java.lang.Class[]{WebConfig.class};
    }

    protected java.lang.String[] getServletMappings() {
        java.lang.String[] strings = {"/"};
        return strings;
    }

}




