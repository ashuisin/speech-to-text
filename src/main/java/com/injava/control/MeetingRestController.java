package com.injava.control;

import com.google.gson.Gson;
import com.injava.model.Meeting;
import com.injava.model.MeetingDAO;
import com.injava.model.Signup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.sql.SQLException;

import java.util.List;

@Controller
@RequestMapping
public class MeetingRestController {

    @Autowired
    private MeetingDAO meetingDAO;
    private String Emailid;
    private String SessionId;
    private String emailid;
    private String document;

    @GetMapping(value = "/getMeetings", produces = "application/json")
    public ResponseEntity<String> getMeetings() {
        System.out.println("getMeetings");
        String response = null;
        try {

            List list = meetingDAO.list();
            response = new Gson().toJson(list);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }


    }

    @GetMapping(value = "/meetings/{Emailid}/{SessionId}", produces = "application/json")
    public ResponseEntity<String> getMeeting(@PathVariable String  Emailid,@PathVariable String SessionId) {
        this.Emailid = Emailid;
        this.SessionId=SessionId;


        Meeting meeting = (Meeting) meetingDAO.get(Emailid,SessionId);
        if (meeting != null) {
            return new ResponseEntity<String>(new Gson().toJson(meeting), HttpStatus.OK);
        }

        return new ResponseEntity<String>("No meetings found with ID" + Emailid, HttpStatus.NOT_FOUND);
    }
    @GetMapping(value="/meetings/{Emailid}",produces = "application/json")
    public ResponseEntity<String> getTotal(@RequestBody Meeting meeting) throws SQLException{
        meetingDAO.getTotal(meeting);
        ResponseEntity<String> entity= new ResponseEntity<String>(new Gson().toJson(meeting),HttpStatus.OK);
        return entity;
    }
    @PostMapping(value = "/meetings", consumes = "application/json")
    public ResponseEntity<String> getMeeting(@RequestBody Meeting meeting) throws IOException {
        System.out.println("meetings"+ " "+meeting);

        if(meetingDAO.create(meeting)) {

            ResponseEntity<String> entity = new ResponseEntity<String>(new Gson().toJson(meeting), HttpStatus.OK);
            if(meeting.getEndtime() != null && meeting.getDocument() != null){
                Meeting.POSTRequest(meeting.getEmailId(), meeting.getDocument());
            }
            return new ResponseEntity<>("Success.", HttpStatus.OK);
        }
       else {
           return new ResponseEntity<String>("No meetings Found for ID" + SessionId, HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping(value="/meetings/{Emailid}/{SessionId}")
    public ResponseEntity<String> deleteMeeting(@PathVariable String Emailid,@PathVariable String SessionId) {
        if (meetingDAO.delete(Emailid,SessionId)) {
             ResponseEntity<String> entity=new ResponseEntity<>(new Gson().toJson(Emailid), HttpStatus.OK);
            return new ResponseEntity<>("Successfully deleted.", HttpStatus.OK);

        } else {
            return new ResponseEntity<String>("No meetings Found for ID" + Emailid, HttpStatus.NOT_FOUND);

        }
    }


    @GetMapping(value = "/getvalues", produces = "application/json")
    public ResponseEntity<String> getvalues() {
        System.out.println("getvalues");
        String response = null;
        try {

            List list1 = meetingDAO.list1();
            response = new Gson().toJson(list1);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }


    }
    @PostMapping(value = "/values", consumes = "application/json")
    public ResponseEntity<String> values(@RequestBody Signup values)  {
        System.out.println("values"+ " "+values);
        if(meetingDAO.createvalues(values)) {
            //ResponseEntity<String> entity = new ResponseEntity<String>(new Gson().toJson(values),HttpStatus.OK);
            return new ResponseEntity<>("Successfully Registered.", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<String>("User already exists,Try signing with other emailid" , HttpStatus.NOT_FOUND);
        }

    }
    @PostMapping(value = "/loginvalues", consumes = "application/json")
    public ResponseEntity<String> logindetails(@RequestBody Signup values)  {
        System.out.println("loginvalues"+ " "+values);
        if(meetingDAO.logindetails(values)) {
          //  ResponseEntity<String> entity = new ResponseEntity<String>(new Gson().toJson(values),HttpStatus.OK);
            return new ResponseEntity<>("Successfully Loggedin.", HttpStatus.OK);

        }
        else {
            return new ResponseEntity<String>("Login failed,Kindly register." , HttpStatus.NOT_FOUND);
        }

    }

    }


