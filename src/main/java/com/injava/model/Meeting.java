package com.injava.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.*;
import java.lang.String;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Blob;


public class Meeting implements Serializable {


    @Expose
    @SerializedName("Emailid")
    private String Emailid;
    @Expose
    @SerializedName("Starttime")
    private String Starttime;
    @Expose
    @SerializedName("Endtime")
    private String Endtime;
    @Expose
    @SerializedName("document")
    private String document;
    @Expose
    @SerializedName("SessionId")
    private String SessionId;
    @Expose
    @SerializedName("Status")
    private String Status;

    public Meeting(String Emailid, String Starttime, String Endtime, String document, String SessionId, String Status) throws IOException {
        this.Emailid = Emailid;
        this.Starttime = Starttime;
        this.Endtime = Endtime;
        this.document = document;
        this.SessionId = SessionId;
        this.Status = Status;
    }
    public Meeting() throws IOException {

    }
    public Meeting(String s) {
    }
    public String getEmailId() {
        return Emailid;
    }

    public void setEmailId(String Emailid) {
        this.Emailid = Emailid;
    }

    public String getStarttime() {
        return Starttime;
    }

    public void setStarttime(String Starttime) {
        this.Starttime = Starttime;
    }

    public String getEndtime() {
        return Endtime;
    }

    public void setEndtime(String Endtime) {
        this.Endtime = Endtime;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }


    public static void POSTRequest(String emailid, String document) {
        try {
            URL url = new URL("http://192.168.1.82:5000/summary");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            String input = "{\"email_id\":\""+emailid+"\",\"document\":\""+document+"\"}";
           OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
  if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "+ conn.getResponseCode());
            }
  else {
      BufferedReader br = new BufferedReader(new InputStreamReader(
              (conn.getInputStream())));
      String output;
      System.out.println("Output from Server .... \n");
      while ((output = br.readLine()) != null) {
          System.out.println(output);
      }
  }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}