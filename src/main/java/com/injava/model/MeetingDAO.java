package com.injava.model;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;

import com.injava.config.Sample;
import org.springframework.stereotype.Repository;


@Repository
public class MeetingDAO implements Sample {

    private static Meeting meeting;
    private String Emailid;
    private String SessionId;

    public MeetingDAO() throws IOException {
    }

    public List list() throws IOException {
        List<Meeting> users = new ArrayList();
        String query1 = "select * from Details";
        Connection connect = null;


        try {
            connect = connect();
            PreparedStatement preparedStatement = connect.prepareStatement(query1);
            ResultSet resultSet = preparedStatement.executeQuery();


            while (resultSet.next()) {
                Meeting cust = new Meeting();

                cust.setEmailId(resultSet.getString(1));
                cust.setStarttime(resultSet.getString(2));
                cust.setEndtime(resultSet.getString(3));
                cust.setDocument(resultSet.getString(4));
                cust.setSessionId(resultSet.getString(5));
                cust.setStatus(resultSet.getString(6));
                users.add(cust);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return users;
    }


    public boolean create(Meeting users) {
        Connection connect = null;
        boolean isStored = false;
        try {
            connect = connect();
            String query0 = "select Emailid , SessionId from Details where Emailid=? and SessionId=?";


            Meeting cust3 = null;

            PreparedStatement preparedStatement = connect.prepareStatement(query0);
            preparedStatement.setString(1, users.getEmailId());
            preparedStatement.setString(2, users.getSessionId());
            ResultSet resultSet1 = preparedStatement.executeQuery();
            while (resultSet1.next()) {
                cust3 = new Meeting();

                cust3.setEmailId(resultSet1.getString(1));
                cust3.setSessionId(resultSet1.getString(2));
            }


            if (cust3 != null) {
                String query11 = "UPDATE Details SET Endtime=?,document=?,Status=? where Emailid=? and SessionId=?";


                PreparedStatement preparedStatement1 = connect.prepareStatement(query11);
                preparedStatement1.setString(1, users.getEndtime());
                preparedStatement1.setString(2, users.getDocument());
                preparedStatement1.setString(3, users.getStatus());
                preparedStatement1.setString(4, users.getEmailId());
                preparedStatement1.setString(5, users.getSessionId());
                preparedStatement1.executeUpdate();
                return true;
            } else {

                String query = "INSERT INTO Details(Emailid,Starttime,SessionId,Status) VALUES(?,?,?,?)";


                PreparedStatement preparedStatement2 = connect.prepareStatement(query);
                preparedStatement2.setString(1, users.getEmailId());
                preparedStatement2.setString(2, users.getStarttime());
                preparedStatement2.setString(3, users.getSessionId());
                preparedStatement2.setString(4, users.getStatus());
                preparedStatement2.execute();
                isStored = true;
                return isStored;
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return isStored;
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    @Override
    public Meeting get(String Emailid, String SessionId) {
        String query2 = "select Starttime,Endtime from Details where Emailid=? and SessionId=?";
        Connection connect = null;
        try {
            connect = connect();
            PreparedStatement prepareStatement = connect.prepareStatement(query2);
            prepareStatement.setString(1, Emailid);
            prepareStatement.setString(2, SessionId);
            ResultSet rs = prepareStatement.executeQuery();
            Meeting cust1 = new Meeting();
            while ((rs.next())) {
                cust1.setStarttime(rs.getString(1));
                cust1.setEndtime(rs.getString(2));

            }
            return cust1;
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public String getTotal(Meeting meeting) {
        String query12 = "select * from Details where Emailid=?";
        Connection connect = null;
        try {
            connect = connect();
            PreparedStatement prepareStatement = connect.prepareStatement(query12);

            prepareStatement.setString(1, Emailid);

            prepareStatement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return null;
    }

    public boolean delete(String Emailid, String SessionId) {
        String query3 = "delete from Details where Emailid=? and SessionId=?";
        Connection connect = null;
        try {
            connect = connect();
            PreparedStatement preparedStatement = connect.prepareStatement(query3);
            preparedStatement.setString(1, Emailid);
            preparedStatement.setString(2, SessionId);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public List list1() {
        List<Signup> values = new ArrayList();
        String que = "select * from Signup";
        Connection connect = null;

        try {
            connect = connect();
            PreparedStatement preparedStatement = connect.prepareStatement(que);
            ResultSet resultSet1 = preparedStatement.executeQuery();


            while (resultSet1.next()) {
                Signup sign = new Signup();

                sign.setName(resultSet1.getString(1));
                sign.setEmail(resultSet1.getString(2));
                sign.setPassword(resultSet1.getString(3));
                values.add(sign);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return values;
    }

    public boolean createvalues(Signup values) {
        Connection connect = null;
        boolean isStored = false;
        try {
            connect = connect();
            String que12 = "Select email from signup where email=?";
            PreparedStatement preparedStatement0 = connect.prepareStatement(que12);
            preparedStatement0.setString(1, values.getEmail());
            ResultSet resultSet00 = preparedStatement0.executeQuery();

            Signup login = null;
            while (resultSet00.next()) {
                login = new Signup();
                login.setEmail(resultSet00.getString(1));
            }
            if (login==null) {
                String que1 = "INSERT INTO signup(name,email,password) VALUES(?,?,?)";
                 PreparedStatement preparedStatement2 = connect.prepareStatement(que1);
                preparedStatement2.setString(1, values.getName());
                preparedStatement2.setString(2, values.getEmail());
                preparedStatement2.setString(3, values.getPassword());
                preparedStatement2.execute();
                isStored = true;
                return isStored;
            }
            else {
            return false;
        }
            }
         catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

        public boolean logindetails(Signup values){
        Connection connect = null;
        try {
            connect = connect();
                String que3 = "Select email,password from signup where email=? and password=?";
                PreparedStatement preparedStatement0 = connect.prepareStatement(que3);
                preparedStatement0.setString(1, values.getEmail());
                preparedStatement0.setString(2, values.getPassword());
                ResultSet resultSet0 = preparedStatement0.executeQuery();


            Signup  login = null;
            while (resultSet0.next()) {
                login = new Signup();
                    login.setEmail(resultSet0.getString(1));
                    login.setPassword(resultSet0.getString(2));

            }
            if (login!=null){
                return true;
            }
            else {
                return false;
            }


        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        finally{
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());

            }
        }
    }

    @Override
        public Connection connect () {
            Connection conn = null;
            try {

                String url = "jdbc:sqlite:C:/sqlite3/meeting.db";

                conn = DriverManager.getConnection(url);

                System.out.println("Connection to SQLite has been established.");

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            return conn;


        }

        public void main (String[]args){
            connect();
        }
    }


