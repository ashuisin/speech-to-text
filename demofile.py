import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import os
from util import get_conf_property


group_email = get_conf_property('email-credentials', 'group_email')


def send_summary_email(email_id,summary_file):
    fromaddr = get_conf_property('email-credentials', 'fromaddr')
    password = get_conf_property('email-credentials', 'password')
    toaddr = email_id, group_email

    print(toaddr)

    msg = MIMEMultipart()

    msg['From'] = fromaddr

    msg['To'] = ",".join(toaddr)

    msg['Subject'] = "IB | MoM"

    body = "Hi,\nPFA document related to the meeting that you have " \
           "attended today in InnovatorsBay.\n\nThank You!"

    msg.attach(MIMEText(body, 'plain'))

    filename = "File_name_with_extension"

    attachment = open("C:/Users/ADMIN/PycharmProject/sampleproject/summary_file.txt")

    p = MIMEBase('application', 'octet-stream')

    p.set_payload((attachment).read())

    encoders.encode_base64(p)

    p.add_header('Content-Disposition', "attachment; filename= %s" % filename)

    msg.attach(p)

    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()

    s.login(fromaddr, password)

    text = msg.as_string()

    s.sendmail(fromaddr, toaddr, text)

    s.quit()
